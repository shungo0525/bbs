## BBS site

# 概要
5chのようなWeb掲示板です。

# HerokuURL
https://bbs-site.herokuapp.com/
下記でログインできます。
(email:"example@bbs.com", password:"password")

# 機能
1. ログイン機能 (email + pass)
2. 閲覧・投稿機能
3. スレッド機能

# 環境
言語: Ruby 2.5.1
フレームワーク: Ruby on rails 6.0.2
DB: sqlite3
テスト: Rspec
コードチェック: rubocop
