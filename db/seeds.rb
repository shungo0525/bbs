User.create!(name:  "Example User",
             email: "example@bbs.com",
             password:              "password",
             password_confirmation: "password")

20.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@bbs.com"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
end

users = User.order(:created_at).take(6)
10.times do
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.posts.create!(title: content) }
end
