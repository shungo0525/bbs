require 'rails_helper'

describe 'users', type: :system do
  describe '新規登録' do
    before do
      visit signup_path
      fill_in 'user[name]', with: 'テストユーザー'
      fill_in 'user[email]', with: 'a@example.com'
      fill_in 'user[password]', with: 'password'
      fill_in 'user[password_confirmation]', with: 'password'
      click_button 'アカウント作成'
    end

    it '正しくユーザー登録できる' do
      expect(page).to have_content "テストユーザー"
      expect(page).to have_content "ユーザー登録しました。"
    end
  end

  describe 'ログイン' do
    let!(:user_a) do
      FactoryBot.create(:user, name: 'ユーザーA',
                               email: 'a@example.com')
    end

    before do
      visit login_path
      fill_in 'session[email]', with: 'a@example.com'
      fill_in 'session[password]', with: 'password'
      click_button 'ログイン'
    end

    it '正しくログインできる' do
      expect(page).to have_content "ログインしました。"
      expect(page).to have_content "ユーザーA"
    end

    it 'ログインしたときにヘッダーが切り替わる' do
      within "nav" do
        expect(page).to have_content "ログアウト"
        expect(page).not_to have_content "ログイン"
      end
    end
  end

  describe 'アカウント変更' do
    let!(:user_a) do
      FactoryBot.create(:user, name: 'ユーザーA',
                               email: 'a@example.com')
    end

    before do
      visit login_path
      fill_in 'session[email]', with: 'a@example.com'
      fill_in 'session[password]', with: 'password'
      click_button 'ログイン'
      visit edit_user_path(user_a)
    end

    it 'アカウント変更ページが表示される' do
      expect(page).to have_content "アカウント変更"
    end

    it 'アカウント変更できる' do
      fill_in 'user[name]', with: 'ユーザーAA'
      click_button '保存'
      expect(page).to have_content "ユーザーAA"
    end

    it 'アカウント削除できる' do
      expect { user_a.destroy }.to change(User, :count).by(-1)
    end
  end

  describe 'ログアウト' do
    let!(:user_a) do
      FactoryBot.create(:user, name: 'ユーザーA',
                               email: 'a@example.com')
    end

    before do
      visit login_path
      fill_in 'session[email]', with: 'a@example.com'
      fill_in 'session[password]', with: 'password'
      click_button 'ログイン'
    end

    it 'ログアウトできる' do
      click_link 'ログアウト'
      expect(page).to have_content "ログアウトしました。"
    end
  end
end
