require 'rails_helper'

describe 'posts', type: :system do
  describe '投稿' do
    context 'ユーザーAでログインしているとき' do
      let!(:user_a) do
        FactoryBot.create(:user, name: 'ユーザーA',
                                 email: 'a@example.com')
      end
      let!(:user_b) do
        FactoryBot.create(:user, name: 'ユーザーB',
                                 email: 'b@example.com')
      end
      let!(:post_b) { FactoryBot.create(:post, title: '投稿_B', user: user_b) }

      before do
        visit login_path
        fill_in 'session[email]', with: 'a@example.com'
        fill_in 'session[password]', with: 'password'
        click_button 'ログイン'
        click_link 'Home'
      end

      it 'ホームページに投稿画面が表示される' do
        expect(page).to have_button "投稿"
        expect(page).to have_content "スレッドタイトル"
      end

      it '他のユーザーの投稿が表示される' do
        expect(page).to have_content "投稿_B"
      end

      it '他のユーザーの投稿は削除できない' do
        click_link "投稿_B"
        expect(page).not_to have_content "削除"
      end

      context '投稿する' do
        before do
          fill_in 'post[title]', with: 'タイトル'
          fill_in 'post[content]', with: '詳細'
          click_button '投稿'
        end

        it '正しく投稿できる' do
          expect(page).to have_content "投稿しました。"
          expect(page).to have_content "タイトル"
        end

        it '投稿の詳細画面が表示される' do
          click_link 'タイトル'
          expect(page).to have_content "タイトル"
          expect(page).to have_content "詳細"
        end
      end
    end
  end
end
