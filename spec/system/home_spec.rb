require 'rails_helper'

describe 'Homeページ', type: :system do
  before do
    visit root_path
  end

  it 'ヘッダーが正しく表示される' do
    expect(page).to have_link "BBS"
    expect(page).to have_link "Home"
    expect(page).to have_link "ログイン"
  end

  it '新規登録へのリンクがある' do
    click_link "新規登録", match: :prefer_exact
    expect(current_path).to eq signup_path
  end

  it 'タイトルが正しく表示される' do
    expect(page).to have_title "Home | BBS"
  end
end
