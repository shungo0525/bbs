require 'rails_helper'

describe 'comments', type: :system do
  describe 'コメント' do
    context 'ユーザーAでログインしているとき' do
      let!(:user_a) do
        FactoryBot.create(:user, name: 'ユーザーA',
                                 email: 'a@example.com')
      end
      let!(:user_b) do
        FactoryBot.create(:user, name: 'ユーザーB',
                                 email: 'b@example.com')
      end
      let!(:post_a) { FactoryBot.create(:post, title: '投稿_A', user: user_a) }
      let!(:post_b) { FactoryBot.create(:post, title: '投稿_B', user: user_b) }

      before do
        visit login_path
        fill_in 'session[email]', with: 'a@example.com'
        fill_in 'session[password]', with: 'password'
        click_button 'ログイン'
        click_link 'Home'
      end

      it 'コメント画面が表示される' do
        click_link "投稿_A"
        expect(page).not_to have_button "コメントする"
      end

      it '自分の投稿にコメントできる' do
        click_link "投稿_A"
        fill_in 'comment[content]', with: 'コメントA'
        click_button "コメントする"
        expect(page).to have_content "コメントA"
      end

      it '他のユーザーの投稿にコメントできる' do
        click_link "投稿_B"
        fill_in 'comment[content]', with: 'コメントC'
        click_button "コメントする"
        expect(page).to have_content "コメントC"
      end
    end
  end
end
