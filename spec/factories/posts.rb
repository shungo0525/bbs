FactoryBot.define do
  factory :post do
    title { 'タイトル' }
    content { '詳細' }
    user
  end
end
